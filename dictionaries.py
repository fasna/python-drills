def word_count(s):
    words_dictionary={}
    for word in s.split():
        if(word[-1].isalpha()):
            pass
        else:
            word=word[0:len(word)-1]
        if word in dict:
            words_dictionary[word]+=1
        else:
            words_dictionary[word]=1


    return dict

def dict_items(d):
    item_list=[]
    for key in d:
        item_list.append((key,d[key]))
    return item_list



def dict_items_sorted(d):
    sorted_dict=[]
    for i in sorted(d):
        sorted_dict.append((i,d[i]))
    return sorted_dict
