"""
Implement the below file operations.
Close the file after each operation.
"""

import csv
def read_file(path):
    f=open(path,"r")
    r=f.read()
    f.close()
    return r


def write_to_file(path, s):
    f=open(path,"w")
    f.write(s)
    f.close()


def append_to_file(path, s):
    f=open(path,"a")
    f.write(s)
    f.close()


def numbers_and_squares(n, file_path):
    f=open(file_path,"w")
    csvwriter = csv.writer(f)
    for i in range(1,n+1):
        csvwriter.writerow([i,i**2])
    f.close()
