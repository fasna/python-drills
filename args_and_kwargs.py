def add3(a, b, c):
    return sum([a, b, c])


def unpacking1():
    items = [1, 2, 3]
    add3(*items)



def unpacking2():
    kwargs = {'a': 1, 'b': 2, 'c': 3}
    add3(**kwargs)


def call_function_using_keyword_arguments_example():
    a = 1
    b = 2
    c = 3
    add3(a=1,c=3,b=2)


def add_n(*nums):
    sum=0
    for number in nums:
        sum+=number
    return sum


def add_kwargs(a=0,b=0):
    return a+b


def universal_acceptor(*args,**kwargs):
    for i in args:
        print(i)
    for key,value in kwargs.items():
        print(key,value)
