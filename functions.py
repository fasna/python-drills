def is_prime(n):
    for i in range(2,int(n/2)+1):
        if(n%i==0):
            return False
    return True

def check_primenums(start,end):
    primes=[]
    for i in range(start,end):
        if(is_prime(i)):
            primes.append(i)
    return primes


def n_digit_primes(digit=2):
    prime_list=[]
    if(digit>1):
        prime_list=check_primenums(10*(digit-1),10**digit)
    elif(digit==1):
        prime_list=check_primenums(2,10)
    return prime_list
