import math
import time


def generic_timer(func):
    b_time,a_time,ex_time=0,0,0
    def wrapper(*args):
        b_time=time.perf_counter()
        a=func(*args)
        a_time=time.perf_counter()
        ex_time=a_time-b_time
        print(ex_time)
    return wrapper

def memoize(func):
    fib_dict={}
    def wrapper(n):
        if n not in fib_dict:
            fib_dict[n]=func(n)
        return fib_dict[n]
    return wrapper

@generic_timer
def sqrt(x):
    return math.sqrt(x)

@generic_timer
def square(x):
    return x * x

@generic_timer
def add(x, y):
    return x + y

@generic_timer
def add3(x, y, z):
    return x + y + z

@generic_timer
def add_any(*args):
    return sum(args)

@generic_timer
def abs_add_any(*args, **kwargs):
    total = sum(args)
    if kwargs.get('abs') is True:
        return abs(total)
    return total


@memoize
def fib(n):
    print("Computing fib({})".format(n))
    if n in [0, 1]:
        return n
    return fib(n - 1) + fib(n - 2)

@memoize
def factorial(n):
    print("Computing factorial({})".format(n))
    fact = 1
    for i in range(1, n + 1):
        fact *= i
    return fact


# a=fib(6)

# sqrt(10)
a=factorial(5)
print(a)
