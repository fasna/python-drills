def integers_from_start_to_end_using_range(start, end, step):
    integer_list=[]
    for i in range(start,end,step):
        integer_list.append(i)
    return integer_list


def integers_from_start_to_end_using_while(start, end, step):
    integer_list=[]
    i=start
    while(start<end and i<end or start>end and i>end):
        integer_list.append(i)
        i+=step
    return integer_list


def two_digit_primes():
    integer_list=[]
    for i in range(10,100):
        for j in range(2,int(i/4)+1):
            if(i%j==0):
                break;
            elif(j==int(i/4)):
                integer_list.append(i)
    return integer_list
