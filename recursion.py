
def html_dict_search(html_dict, selector):
    lst=[]
    selector=selector[1:]
    def search_tree(dict,selector):
        for key in dict:
            if (key=="attrs"):
                for attrskeys in dict["attrs"]:
                    if "class" in dict["attrs"]:
                        if (dict["attrs"]["class"]==selector):
                            lst.append(dict)
                    if "id" in dict["attrs"]:
                        if (dict["attrs"]["id"]==selector):
                            lst.append(dict)
            if(key=="children"):
                for item in dict[key]:
                    search_tree(item,selector)
    search_tree(html_dict,selector)
    return lst
